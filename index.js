const elList = document.querySelector("#list")
const btnAdd = document.querySelector(".add__item");
let timeSecond = document.querySelector(".timer__second")
let timeMinut = document.querySelector(".timer__minut")
let startTime = document.querySelector("#start");
let pauseTime = document.querySelector("#pause");
let resetTime = document.querySelector("#reset");
let interval;

let saveMinut = window.sessionStorage.getItem("saveMinut")
let saveSecond = window.sessionStorage.getItem("saveSecond")

let counterSecond = saveSecond ? saveSecond : 0;
let counterMinut = saveMinut ? saveMinut : 0;
let second = 0;
let minut = 0;


let localSave = JSON.parse(window.localStorage.getItem('addArray'));
let addArrayTimer = [];

if (localSave) {
   console.log(localSave);
   localSave.forEach((elem) => {
      const elItem = document.createElement("li");
      const elDelete = document.createElement("button");
      
      elDelete.classList.add("delete__btn");
      elItem.textContent = elem.text;
      
      elDelete.textContent = "Delete";
      elDelete.dataset.uuid = elem.id;
      elItem.classList.add("list__item");
      
      elItem.append(elDelete);
      elList.append(elItem);
      
      elDelete.addEventListener("click", deleteButton)
      // window.localStorage.setItem('addArray', JSON.stringify(addArrayTimer))
   })
}

function deleteButton(e) {
   const delId = e.target.dataset.uuid;
   const findArray = addArrayTimer.findIndex((id) => id.id == delId);
   addArrayTimer.splice(findArray, 1);
   
   renderTimer();
   window.localStorage.setItem('addArray', JSON.stringify(addArrayTimer))
}

function renderTimer() {
   elList.innerHTML = '';
   
   addArrayTimer.forEach((elem) => {
      const elItem = document.createElement("li");
      const elDelete = document.createElement("button");
      
      elDelete.classList.add("delete__btn");
      elItem.textContent = elem.text;
      
      elDelete.textContent = "Delete";
      elDelete.dataset.uuid = elem.id;
      elItem.classList.add("list__item");
      
      elItem.append(elDelete);
      elList.append(elItem);
      
      elDelete.addEventListener("click", deleteButton)
      // window.localStorage.setItem('addArray', JSON.stringify(addArrayTimer))
   })
   
}


btnAdd.addEventListener("click", (e) => {
   addArrayTimer.push({
      id: new Date().getTime(),
      text: `${minut.toString().padStart(2, "0")}:${second.toString().padStart(2, "0")}`
   })
   renderTimer()
   window.localStorage.setItem('addArray', JSON.stringify(addArrayTimer))
   // console.log(localSave);
})

timeMinut.textContent = counterMinut.toString().padStart(2, "0");
timeSecond.textContent = counterSecond.toString().padStart(2, "0");
pauseTime.style.display = 'none'

startTime.addEventListener("click", () => {
   pauseTime.style.display = 'inline-block'
   startTime.style.display = 'none'
   clearInterval(interval)
   interval = setInterval(startTimer, 1000);
   
   btnAdd.disabled = false;
});

pauseTime.addEventListener("click", () => {
   startTime.style.display = 'inline-block'
   pauseTime.style.display = 'none'
   clearInterval(interval)
   window.sessionStorage.setItem("saveSecond", counterSecond)
   window.sessionStorage.setItem("saveMinut", counterMinut)
});


resetTime.addEventListener("click", () => {
   const question = confirm('Are you sure to reset?');
   if (question === true) {
      elList.innerHTML = null
      counterSecond = 0;
      counterMinut = 0;
      
      second = 0;
      minut = 0;
      clearInterval(interval)
      
      counterMinut = counterMinut.toString().padStart(2, "0");
      
      timeMinut.textContent = counterMinut.toString().padStart(2, "0");
      timeSecond.textContent = counterSecond.toString().padStart(2, "0");
      
      window.sessionStorage.removeItem("saveSecond");
      window.sessionStorage.removeItem("saveMinut");
      
      window.localStorage.removeItem("addArray");
      addArrayTimer.splice(0, addArrayTimer.length)
   }
});


function startTimer() {
   counterSecond++;
   second++
   if (second === 60) {
      second = 0;
      minut++;
   }
   if (counterSecond === 60) {
      counterSecond = 0;
      counterMinut++;
   }
   counterMinut = counterMinut.toString().padStart(2, "0");
   timeMinut.textContent = counterMinut;
   counterSecond = counterSecond.toString().padStart(2, "0");
   
   timeSecond.textContent = counterSecond;
}

// toString.padStart(23, "0")
// 1- 9 = 01 - 00000000000000009000000